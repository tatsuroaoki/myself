let firstNameButton = document.getElementById('firstNameButton');
firstNameButton.addEventListener('click', myFirstName);

let lastNameButton = document.getElementById('lastNameButton');
lastNameButton.addEventListener('click', myLastName);

let birthplaceButton = document.getElementById('birthplaceButton');
birthplaceButton.addEventListener('click', myBirthplace);

let collegeButton = document.getElementById('collegeButton');
collegeButton.addEventListener('click', myCollege);

let majorButton = document.getElementById('majorButton');
majorButton.addEventListener('click', myMajor);

let employerButton = document.getElementById('employerButton');
employerButton.addEventListener('click', myEmployer);

let kidsButton = document.getElementById('kidsButton');
kidsButton.addEventListener('click', myKids);

// description of myself
let myself = {
	firstName: 'Tatsuro',
	lastName: 'Aoki',
	birthplace: 'Japan',
	college: 'University of Washington',
	major: 'Industrial Engineering',
	employer: 'Boeing',
	hasKids: true
}

function myFirstName() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.firstName;
}

function myLastName() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.lastName;
}

function myBirthplace() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.birthplace;
}

function myCollege() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.college;
}

function myMajor() {
	let answer = document.getElementById('answer')
	answer.innerHTML = myself.major;
}

function myEmployer() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.employer;
}

function myKids() {
	let answer = document.getElementById('answer');
	answer.innerHTML = myself.hasKids;
}